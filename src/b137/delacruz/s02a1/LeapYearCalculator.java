package b137.delacruz.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args) {
        //System.out.println("Leap Year Calculator");

        //Scanner appScanner = new Scanner(System.in);

        // System.out.println("What is your firstname?\n");
        // String firstName = appScanner.nextLine();
        // System.out.println("Hello, " + firstName + "!\n");

        // Activity: Create a program that check if a year is a leap year of not.
        //  1999 != leap year
        // 2000 = leap year
        // 2004 = leap year

        int year;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter any Year:");
        year = scan.nextInt();
        scan.close();
        boolean isLeap = false;

        if(year % 4 == 0)
        {
            if( year % 100 == 0)
            {
                if ( year % 400 == 0)
                    isLeap = true;
                else
                    isLeap = false;
            }
            else
                isLeap = true;
        }
        else {
            isLeap = false;
        }

        if(isLeap==true)
            System.out.println(year + " is a Leap Year.");
        else
            System.out.println(year + " is not a Leap Year.");

    }
}
